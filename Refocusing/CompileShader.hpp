//
//  CompileShader.hpp
//  DepthBasedRefocusing
//
//  Created by YangYang on 9/28/16.
//  Copyright © 2016 YangYang. All rights reserved.
//

#pragma once

#include <GL/glew.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <GLFW/glfw3.h>

class CompileShader
{
public:
    GLuint Program;
    
    CompileShader(const GLchar* vertexPath, const GLchar* fragmentPath);
    
    void UseProgram()
    {
        glUseProgram(this->Program);
    }
    
private:
    void CheckCompileErrors(GLuint shader, std::string type);
};
