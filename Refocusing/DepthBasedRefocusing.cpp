//
//  DepthBasedRefocusing.cpp
//  DepthBasedRefocusing
//
//  Created by YangYang on 9/28/16.
//  Copyright © 2016 YangYang. All rights reserved.
//

#include "DepthBasedRefocusing.hpp"

DepthBasedRefocusing::DepthBasedRefocusing():
					  m_textureBindwithFBWidth(960),
					  m_textureBindwithFBHeight(540),
					  m_focalDepth(1.0),
					  m_kernelRadius(15.0)
{
	
}

DepthBasedRefocusing::~DepthBasedRefocusing()
{

}

bool DepthBasedRefocusing::LoadTexture(const char* fileName, GLuint* tex, bool isDepth)
{	
	cv::Mat image; 
	
	if (isDepth)
	{
		image = cv::imread(fileName, CV_LOAD_IMAGE_GRAYSCALE);
		//cv::imshow("Display", image);
		//cv::waitKey();
		cvtColor(image, image, CV_GRAY2RGB);
	}
	else
	{
		image = cv::imread(fileName, CV_LOAD_IMAGE_COLOR);
		cvtColor(image, image, CV_BGR2RGB);
	}
	cv::flip(image, image, 0);
	
	
	int width = image.cols;
	int height = image.rows;

	unsigned char* imageData = image.data;
	
	if (!imageData)
	{
		fprintf (stderr, "ERROR: could not load %s\n", fileName);
		return false;
	}
	
	glGenTextures (1, tex);
	glBindTexture (GL_TEXTURE_2D, *tex);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
	
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	
	int error = glGetError();
	
	assert(error == GL_NO_ERROR);

	return true;
}

bool DepthBasedRefocusing::InitailizeFrameBuffer()
{
	glGenFramebuffers(1, &m_frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
	
	glGenTextures(1, &m_textureBindwithFB);
	glBindTexture(GL_TEXTURE_2D, m_textureBindwithFB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_textureBindwithFBWidth, m_textureBindwithFBHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_textureBindwithFB, 0);
	
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	
	if (GL_FRAMEBUFFER_COMPLETE != status)
	{
		fprintf (stderr, "ERROR: incomplete framebuffer\n");
		return false;
	}
	
	glBindFramebuffer (GL_FRAMEBUFFER, 0);
	
	int error = glGetError();
	
	assert(error == GL_NO_ERROR);

	return true;
}

void DepthBasedRefocusing::InitailizeGLFW()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	
	m_window = glfwCreateWindow(m_textureBindwithFBWidth, m_textureBindwithFBHeight, "DepthBasedRefocusing", nullptr, nullptr);
	glfwMakeContextCurrent(m_window);

	glewExperimental = GL_TRUE;
	glewInit();

	const char* fragmentPath = "LensBlur.fs";
	const char* vertexPath = "Vertex.vs";

	m_shaderCompiler = new CompileShader(vertexPath, fragmentPath);
}

void DepthBasedRefocusing::PrepareTextures(const char* colorImagePath, const char* depthImagePath)
{
	
	LoadTexture(colorImagePath, &m_textureColorForRender, false);
	LoadTexture(depthImagePath, &m_textureDepthForRender, true);
	
	m_textureColorTable = GenerateColorTable();
	
	const char* lensKernelPath = "laCircular.png";
	LoadTexture(lensKernelPath, &m_textureLensKernel, false);
}

void DepthBasedRefocusing::InitailizeRenderer(const char* colorImagePath, const char* depthImagePath)
{
	glViewport(0, 0, m_textureBindwithFBWidth, m_textureBindwithFBHeight);

	GLfloat vertex[] =
	{
		-1.0f,  -1.0f,  0.0f,
		 1.0f,  -1.0f,  0.0f,
		-1.0f,   1.0f,  0.0f,
		 1.0f,   1.0f,  0.0f
	};
	
	GLfloat textureCoord[] =
	{
		0.0f,   0.0f,
		1.0f,   0.0f,
		0.0f,   1.0f,
		1.0f,   1.0f
	};
	
	GLuint vboVertex;
	glGenBuffers(1, &vboVertex);
	glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);
	
	GLuint vboTextureCoord;
	glGenBuffers(1, &vboTextureCoord);
	glBindBuffer(GL_ARRAY_BUFFER, vboTextureCoord);
	glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoord), textureCoord, GL_STATIC_DRAW);
	
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	
	glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glBindBuffer(GL_ARRAY_BUFFER, vboTextureCoord);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	
	glBindVertexArray(0);
	
	int error = glGetError();
	
	//assert(error == GL_NO_ERROR);
	
	PrepareTextures(colorImagePath, depthImagePath);
	
	InitailizeFrameBuffer();
}

kmMat4 DepthBasedRefocusing::GetViewMatrix()
{
	kmMat4 projection;
	kmMat4 view;
	kmMat4 model;
	kmMat4 MVP;
	
	kmVec3 eye    = {0.0, 0.0, 1.0};
	kmVec3 center = {0.0, 0.0, 0.0};
	kmVec3 up     = {0.0, 1.0, 0.0};

	kmMat4OrthographicProjection(&projection, -1.0f, 1.0f, -1.0f, 1.0f, -0.5f, 100.0f);
	kmMat4LookAt(&view, &eye, &center, &up);
	kmMat4Identity(&model);
	
	kmMat4Multiply(&MVP, &projection, &view);
	kmMat4Multiply(&MVP, &MVP, &model);
	
	return MVP;
}

//void DepthBasedRefocusing::Render(const char* imageSavePath)
void DepthBasedRefocusing::Render(const char* imageSavePath, const float focalDepth, const float kernelRadius)
{
	glfwPollEvents();
	
	//glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
	
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	m_shaderCompiler->UseProgram();
	
	GLuint program = m_shaderCompiler->Program;
	
	kmMat4 MVP = GetViewMatrix();
	
	this->m_focalDepth = focalDepth;

	this->m_kernelRadius = kernelRadius;

	glBindVertexArray(m_VAO);
	
	glUniformMatrix4fv(glGetUniformLocation(program, "MVP"), 1, GL_FALSE, (GLfloat *)&MVP);
	
	glUniform1i(glGetUniformLocation(program, "texture_width"),  m_textureBindwithFBWidth);
	glUniform1i(glGetUniformLocation(program, "texture_height"), m_textureBindwithFBHeight);
	
	glUniform1i(glGetUniformLocation(program, "output_width"),   m_textureBindwithFBWidth);
	glUniform1i(glGetUniformLocation(program, "output_height"),  m_textureBindwithFBHeight);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_textureColorTable);
	glUniform1i(glGetUniformLocation(program, "ColorTable"), 0);
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_textureColorForRender);
	glUniform1i(glGetUniformLocation(program, "Texture"),    1);
	
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_textureDepthForRender);
	glUniform1i(glGetUniformLocation(program, "Depth"),      2);
	
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_textureLensKernel);
	glUniform1i(glGetUniformLocation(program, "LensKernel"), 3);
	
	glUniform1f(glGetUniformLocation(program, "focal_depth"),   m_focalDepth);
	
	glUniform1f(glGetUniformLocation(program, "kernel_radius"), m_kernelRadius);
	
	int error = glGetError();
	
	assert(error == GL_NO_ERROR);
	
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	glBindTexture(GL_TEXTURE_2D, m_textureBindwithFB);
	
	glBindVertexArray(0);
	
	cv::Mat imageFromFramebuffer(m_textureBindwithFBHeight, m_textureBindwithFBWidth, CV_8UC3);
	
	glReadPixels(0, 0, m_textureBindwithFBWidth, m_textureBindwithFBHeight, GL_RGB, GL_UNSIGNED_BYTE, imageFromFramebuffer.data);
	
	cv::flip(imageFromFramebuffer, imageFromFramebuffer, 0);
	cvtColor(imageFromFramebuffer, imageFromFramebuffer, CV_RGB2BGR);
	
	imwrite(imageSavePath, imageFromFramebuffer);
	
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLuint DepthBasedRefocusing::GenerateColorTable()
{
	float* colorTableData = new float[256];
	
	float emphasizeHL = 2.0;
	float threshod    = 255;
	GLint level       = 0;
	
	assert(colorTableData);
	
	const float clr_factor = emphasizeHL / 255.0f / 255.0f;
	
	for(int i = 0; i < 256; i++)
	{
		float dist_intensity   = fmin(0.0f, ((float)i) - threshod);
		
		float weight_intensity = exp(-dist_intensity * dist_intensity * clr_factor);
		
		colorTableData[i]      = weight_intensity;
	}
	
	GLuint texObj;
	glGenTextures(1, &texObj);
	glBindTexture(GL_TEXTURE_2D, texObj);
	
	GLsizei width = 256;
	GLsizei height = 1;
	
	glTexImage2D(GL_TEXTURE_2D, level, GL_RED, width, height, 0, GL_RED, GL_FLOAT, colorTableData);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	
	delete [] colorTableData;
	
	int error = glGetError();
	
	assert(error == GL_NO_ERROR);
	
	return texObj;
}

void DepthBasedRefocusing::GetTextureData(GLuint tex, unsigned char* textureData)
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	glUseProgram(0);
	
	glBindTexture(GL_TEXTURE_2D, tex);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
	
	int error = glGetError();
	
	assert(error == GL_NO_ERROR);
}

void DepthBasedRefocusing::Release()
{
	glDeleteVertexArrays(1, &m_VAO);
	
	glDeleteTextures(1,     &m_textureLensKernel);
	glDeleteTextures(1,     &m_textureColorTable);
	glDeleteTextures(1,     &m_textureBindwithFB);
	
	glDeleteTextures(1,     &m_textureColorForRender);
	glDeleteTextures(1,     &m_textureDepthForRender);

	glDeleteFramebuffers(1, &m_frameBuffer);
	
	int error = glGetError();
	
	assert(error == GL_NO_ERROR);
}
