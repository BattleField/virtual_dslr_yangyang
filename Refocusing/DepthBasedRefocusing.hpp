//
//  DepthBasedRefocusing.hpp
//  DepthBasedRefocusing
//
//  Created by YangYang on 9/28/16.
//  Copyright © 2016 YangYang. All rights reserved.
//

#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "kazmath/kazmath.h"
#include "CompileShader.hpp"

class DepthBasedRefocusing
{
public:
    DepthBasedRefocusing();
    
    virtual ~DepthBasedRefocusing();
    
    bool    LoadTexture(const char* fileName, GLuint* tex, bool isDepth);
    
    bool    InitailizeFrameBuffer();
    
    void    InitailizeRenderer(const char* colorImagePath, const char* depthImagePath);
    
    void    PrepareTextures(const char* colorImagePath, const char* depthImagePath);
    
    void    GetTextureData(GLuint tex, unsigned char* textureData);
    
    void    InitailizeGLFW();
    
    GLuint  GenerateColorTable();
    
    kmMat4  GetViewMatrix();
    
    void    Release();
    
    //void    Render(const char* imageSavePath);

    void Render(const char* imageSavePath, const float focalDepth, const float kernelRadius);
    
    GLFWwindow* m_window;

private:
    
    int            m_textureBindwithFBWidth;
    int            m_textureBindwithFBHeight;
    
    GLuint         m_frameBuffer;
    GLuint         m_textureBindwithFB;
    GLuint         m_textureColorTable;
    GLuint         m_textureColorForRender;
    GLuint         m_textureDepthForRender;
    GLuint         m_textureLensKernel;
    
    GLuint         m_VAO;
    
    CompileShader* m_shaderCompiler;
    
    float m_focalDepth;
    float m_kernelRadius;
};
