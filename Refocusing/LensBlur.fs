#version 330 core

in highp vec2 TextCoordOut;

uniform sampler2D Texture;
uniform sampler2D Depth;
uniform sampler2D LensKernel;
uniform sampler2D ColorTable;

uniform int texture_width;
uniform int texture_height;

uniform int output_width;
uniform int output_height;

uniform mediump float kernel_radius;
uniform highp float focal_depth;

out highp vec4 g_FragColor;

void main()
{
    highp float epsilon = 1e-5;
    
    mediump float w = float(texture_width);
    mediump float h = float(texture_height);
    
    highp float w_out = float(output_width);
    highp float h_out = float(output_height);
    
    lowp vec4 bgTex    = texture(Texture, TextCoordOut);
    lowp vec4 depthTex = texture(Depth, TextCoordOut, 0.0);
    
    mediump float radius = kernel_radius;
    
    mediump float focus = focal_depth;
    
    //factor for computing color distance
    //const highp float emphasizeHL = 4.0;
    //const mediump float threshold = 255.0;
    //const highp float clr_factor = emphasizeHL/255.0/255.0;
    
    highp vec3 values = vec3(0,0,0);
    
    //avoid divided by zero error
    highp vec3 total_weights = vec3(epsilon,epsilon,epsilon);
    
    highp float apertureFactor = 1.0;
    
    //radius = max(2.0,min(kernel_radius, defocus_sigma*3.0));
    
    //Loop over all pixels on the filter kernel
    for(highp float y = -radius; y <= radius; y++)
    {
        for(highp float x = -radius; x <= radius; x++)
        {
            highp vec2 currentTexCoord = vec2(gl_FragCoord.r*w/w_out+x, gl_FragCoord.g*h/h_out+y);
            
            lowp vec4 kernelWeight = texture(LensKernel, vec2((x+radius)/(1.0+2.0*radius), (y+radius)/(1.0+2.0*radius)));
            
            //Outside the lens aperture
            if(abs(kernelWeight.r)<epsilon)
            {
                continue;
            }
            
            //Out of bound
            if(currentTexCoord.r<0.0||currentTexCoord.r>=w||currentTexCoord.g<0.0||currentTexCoord.g>=h)
            {
                continue;
            }
            
            currentTexCoord = vec2((currentTexCoord.r+0.5)/w, (currentTexCoord.g+0.5)/h);
            
            lowp vec4 depth = texture(Depth, currentTexCoord);
            
            highp float distance = (x*x+y*y);
            
            //Case 1: Defocus kernel size
            highp float defocus_sigma = abs(focus-depthTex.r);
            
            //            //Case 2: do not sample pixels at foreground that are in focus
            //            //Other pixels will get sampled according to the defocus kernel
            //            highp float local_defocus_sigma = max(epsilon,(max(0.0, depthTex.r-depth.r)+abs(focus-depth.r))*radius);
            
            highp float sigma = 0.0;//defocus_sigma*apertureFactor;
            
            //Explain
            if(depth.r-depthTex.r>=0.0)
            {
                lowp float foreground_sigma = abs(focus-depth.r);
            
                sigma = foreground_sigma*apertureFactor;
            }
            else
            {
               sigma = defocus_sigma*apertureFactor;
            }
            
            sigma = min(1.0, sigma);
            
            //highp float foreground_weight = exp(-distance/local_defocus_sigma/local_defocus_sigma);
            //highp float sigma = min(local_defocus_sigma, defocus_sigma)*apertureFactor;
            highp float defocus_weight = 1.0;//exp(-distance*4.5/sigma/sigma);
            //highp float local_defocus_weight = exp(-distance/max(epsilon,local_defocus_sigma*local_defocus_sigma));
            
            //defocus_weight = max(epsilon, defocus_weight*local_defocus_weight);
            highp vec2 new_coord = vec2(gl_FragCoord.r*w/w_out+x*sigma+0.5, gl_FragCoord.g*h/h_out+y*sigma+0.5);
            
            highp vec4 clr = texture(Texture, vec2(new_coord.r/w, new_coord.g/h));
            
            //r channel
            lowp vec4 weights = texture(ColorTable, vec2(clr.r, 0), 0.0);
            weights.r = defocus_weight*weights.r;
            total_weights.r+=weights.r;
            values.r+=weights.r*clr.r;
            
            //g channel
            weights = texture(ColorTable, vec2(clr.g, 0), 0.0);
            weights.r = defocus_weight*weights.r;
            total_weights.g+=weights.r;
            values.g+=weights.r*clr.g;
            
            //b channel
            weights = texture(ColorTable, vec2(clr.b, 0), 0.0);
            weights.r = defocus_weight*weights.r;
            total_weights.b+=weights.r;
            values.b+=weights.r*clr.b;
        }
    }
    
    g_FragColor = vec4(values.r/total_weights.r, values.g/total_weights.g, values.b/total_weights.b, 1);
}
