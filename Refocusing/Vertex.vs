#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
uniform mat4 MVP;
out highp vec2 TextCoordOut;

void main()
{
    TextCoordOut = texcoord;
    gl_Position = MVP * vec4(position, 1.0);
}