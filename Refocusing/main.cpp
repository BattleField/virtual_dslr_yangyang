//
//  main.cpp
//  DepthBasedRefocusing
//
//  Created by YangYang on 9/28/16.
//  Copyright © 2016 YangYang. All rights reserved.
//

#include <iostream>
#include "DepthBasedRefocusing.hpp"
#include <math.h>
#include <random>
#include <ctime>

int main(int argc, const char * argv[])
{
	DepthBasedRefocusing *depthBasedRefocusing = new DepthBasedRefocusing;
	
	const int imageNum = 751;
	
	char colorImageLPath[256];
	char colorImageRPath[256];
	
	char depthImageLPath[256];
	char depthImageRPath[256];
	
	char imageSaveLPath[256];
	char imageSaveRPath[256];
	
	depthBasedRefocusing->InitailizeGLFW();
	
	float focalDepth = 0.0f;
	float kernelRadius = 0.0f;
	
	const int stepNum = 2;
	const float step = 1.0 / stepNum;

	for(int i = 490; i < imageNum; i++)
	{
		//left image
		sprintf(colorImageLPath, "H:/Data/flyingthings3d/train/%03d_color_L.png", i);
		sprintf(depthImageLPath, "H:/Data/flyingthings3d/train/%03d_gauss_depth_L.png", i);
		
		//right image
		sprintf(colorImageRPath, "H:/Data/flyingthings3d/train/%03d_color_R.png", i);
		sprintf(depthImageRPath, "H:/Data/flyingthings3d/train/%03d_gauss_depth_R.png", i);
		
		//generate random focal depth and kernel radius
		//srand( time_t(0) );
		kernelRadius = rand() % 4 + 3;
		std::cout << "Shows a random number between 4-7: " << kernelRadius << std::endl;
		
		focalDepth = 0.0; 

		/*for(int j = 0; j < stepNum; j++)
		{
			//focalDepth = ((double)rand() / (RAND_MAX));
		   // std::cout << "Shows a random number between 0-1: " << focalDepth << std::endl;
			
			focalDepth += step; 
			sprintf(imageSaveLPath, "H:/Data/flyingthings3d/results/test/%03d_%03d_L_image.png", i, j);
			sprintf(imageSaveRPath, "H:/Data/flyingthings3d/results/test/%03d_%03d_R_image.png", i, j);
			
			depthBasedRefocusing->InitailizeRenderer(colorImageLPath, depthImageLPath);
			
			//render once
			//        while(!glfwWindowShouldClose(depthBasedRefocusing->m_window)) // while loop render
			//        {
			depthBasedRefocusing->Render(imageSaveLPath, focalDepth, kernelRadius);
			depthBasedRefocusing->Release();
			depthBasedRefocusing->InitailizeRenderer(colorImageRPath, depthImageRPath);
			
			depthBasedRefocusing->Render(imageSaveRPath, focalDepth, kernelRadius);
			depthBasedRefocusing->Release();
			glfwSwapBuffers(depthBasedRefocusing->m_window);
			//        }
			
		}*/

		for (int j = 0; j < stepNum; j++)
		{
			if (j == 0)
			{
				focalDepth = rand() % 30 / 100.0f + 0.1;
				sprintf(imageSaveLPath, "H:/Data/flyingthings3d/results/train/%03d_%03d_L_image.png", i, j);
				sprintf(imageSaveRPath, "H:/Data/flyingthings3d/results/train/%03d_%03d_R_image.png", i, j);
				std::cout << "Shows a random number between 0.1-0.4: " << focalDepth << std::endl;

			}
			else if (j == 1)
			{
				focalDepth = rand() % 30 / 100.0f + 0.6;
				sprintf(imageSaveLPath, "H:/Data/flyingthings3d/results/train/%03d_%03d_L_image.png", i, j);
				sprintf(imageSaveRPath, "H:/Data/flyingthings3d/results/train/%03d_%03d_R_image.png", i, j);
				std::cout << "Shows a random number between 0.6-0.9: " << focalDepth << std::endl;
			}
			
			depthBasedRefocusing->InitailizeRenderer(colorImageLPath, depthImageLPath);
			depthBasedRefocusing->Render(imageSaveLPath, focalDepth, kernelRadius);
			depthBasedRefocusing->Release();

			depthBasedRefocusing->InitailizeRenderer(colorImageRPath, depthImageRPath);
			depthBasedRefocusing->Render(imageSaveRPath, focalDepth, kernelRadius);
			depthBasedRefocusing->Release();

			glfwSwapBuffers(depthBasedRefocusing->m_window);			
		}
	}
	
	
	
	if (GLFW_PRESS == glfwGetKey (depthBasedRefocusing->m_window, GLFW_KEY_ESCAPE))
	{
		glfwSetWindowShouldClose(depthBasedRefocusing->m_window, 1);
	}
	
	glfwTerminate();
	return 0;
}
